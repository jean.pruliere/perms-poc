import checkPermission from '@acme/perms'
import plusOne from './component-one'
import triple from './component-two'

export default function chain(n: number): number {
    // valid call
    if (!checkPermission('chain_permission')) throw new Error('NOPE')

    return triple(plusOne(n))
}