import checkPermission from '@acme/perms'

export default function triple(n: number): number {
    // valid call
    if (!checkPermission('triple_permission')) throw new Error('NOPE')
    if (!checkPermission('some_other_permission')) throw new Error('still no')
    if (!checkPermission('one_last_permission')) throw new Error('that was close but no')

    return n * 3
}