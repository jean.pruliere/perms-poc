import chkprm from '@acme/perms'

export default function plusOne(n: number): number {
    // valid call
    if (!chkprm('plus_one_permission')) throw new Error('NOPE')

    return n + 1
}