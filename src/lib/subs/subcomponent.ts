import checkPermission from '@acme/perms'

export default function identity(n: number): number {
    // valid call
    if (!checkPermission('identity_permission')) throw new Error('NOPE')

    return n
}