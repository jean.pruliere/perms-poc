import haha from '@acme/perms'

export default function plusOne(n: number): number {
    const haha = (s: string): boolean => true

    // this should not be reported
    if (!haha('plus_one_permission')) throw new Error('NOPE')

    return n + 1
}