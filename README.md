# Permissions auto-updates PoC

## Context

As a new permission system is about to be implemented, we need to think about how it will evolve in a distributed system like ours.
IMO, engineers from every other teams who want to restrict access to a feature should have 0 knowledge of how permissions are handled on our part. They require a package (let's call it `@acme/perms`), call its function with a permission label and probably the context object upon which the decision is made, it returns a boolean to allow or deny access. That's it.

## Issue

But then, the question of how a new permission can be created remains. This PoC shows how new perm registration could be done with a script that detects every call of the `@acme/perms` function using `ts-morph` and find the label it is called with. The script can be run at merge time in a Gitlab pipeline for instance and it would call an internal registration API. For security purposes, we could also log/store some info surrounding each registration:

- Date and time
- Assignee of the merged PR
- Repository and branch name

## How to play with it

The `perms` folder only contains a mock function for permission check that always return true, it is then set as a local dependency (`@acme/perms`) in the project to emulate an actual dependency. You can ignore it, implementation is not the subject here.

In `src`, you will find some random arrangement of folders and files, some of the latter contain calls to the permission check function: feel free to add/edit/remove some files or calls.

The detection script is `test.ts` at root level, it exports an array of calls, each consisting of an plain object with a `file` and a `perm` property. Import it from a REPL (or tweak it to your needs) to look it up.

```sh
$ yarn ts-node
> import { calls } from './test'
> calls
[
  {
    file: '/path/to/project/src/lib/component-one.ts',
    perm: "'plus_one_permission'"
  },
  {
    file: '/path/to/project/src/lib/component-three.ts',
    perm: "'chain_permission'"
  },
  {
    file: '/path/to/project/src/lib/component-two.ts',
    perm: "'triple_permission'"
  },
  {
    file: '/path/to/project/src/lib/component-two.ts',
    perm: "'some_other_permission'"
  },
  {
    file: '/path/to/project/src/lib/component-two.ts',
    perm: "'one_last_permission'"
  },
  {
    file: '/path/to/project/src/lib/subs/subcomponent.ts',
    perm: "'identity_permission'"
  }
]
```

## What this PoC is not

- This PoC does not cover Scala code, but I am confident there is a Scala AST parser to do a similar job
- It does not cover the registration API part, how it should work, which endpoint/query/mutation should be available
- It does not provide a real permission checking implementation, the function is a mock
- It calls for improvements: if you think the base idea is cool, do not hesitate to iterate on it
