import { Project, SyntaxKind } from 'ts-morph';

const packageName = '@acme/perms';

const project = new Project();
project.addSourceFilesAtPaths('src/**/*.ts'); // can be set in each project to ease the script processing

// only look in files importing our perms tool
const relevantFiles = project.getSourceFiles()?.filter((f) => f.getImportDeclaration(packageName));

// relevantFiles ==
// [
//     'component-arf.ts', // the import name is shadowed, so the function is not actually called
//     'component-one.ts', // import is named c-styled (should not matter)
//     'component-two.ts', // import is named in camelCase convention, it also contains several calls
//     'component-three.ts', // imports the other two "components" as well (does not matter either)
//     'subcomponent.ts' // is in a sub-folder
// ]

// find calls to the perm check
export const calls = relevantFiles.map((file) => {
    // get local name of the import
    const localSymbol = file.getImportDeclaration(packageName)?.getDefaultImport()?.getSymbol();

    // filter out possible shadowed calls by searching for the same symbol
    const permCalls = file.getDescendantsOfKind(SyntaxKind.CallExpression).filter((ce) => ce.getExpression().getSymbol() === localSymbol);

    // returns the matches
    return permCalls.map((c) => ({
        file: c.getSourceFile().getFilePath(),
        perm: c.getArguments()[0].getText()
    }));
}).flat();
